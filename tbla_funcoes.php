<?php

$app->post("/admin/logar", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM admin WHERE (login = :login AND senha = :senha)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':login' => $data['login'],
		':senha' => $data['senha']
	);

	$stmt->execute($colParams);
	$retorno = $stmt->fetch();

	if ($retorno) {

		// Recupera a nova sessão
		$token = gerarSessao($retorno->id);

		// Atualiza o ultimo acesso
		atualizar_ultimo_acesso($retorno->id);

		echo json_encode(array("retorno" => $retorno, "token" => $token));
	} else {
		return $response->withJson([
			'error'=>'Nenhum registro encontrado',
			'code'=>404
		], 404);
	}

	exit();
});

function gerarSessao($id_admin)
{
	$sql = "INSERT INTO sessao (codigo, data_limite, data_entrada, id_admin) VALUES (:codigo, :data_limite, :data_entrada, :id_admin)";
	$stmt = DB::prepare($sql);

	$token = gerarCodigo();

	$colParams = array(
		':codigo' => $token,
		':data_limite' => date("Y-m-d H:i:s", time() + 3600),
		':data_entrada' => date("Y-m-d H:i:s", time()),
		':id_admin' => $id_admin
	);

	$stmt->execute($colParams);

	return $token;
}

function atualizar_ultimo_acesso($id_admin)
{
	$sql = "UPDATE admin SET data_ultimo_acesso = :data_ultimo_acesso WHERE (id = :id)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':data_ultimo_acesso' => date("Y-m-d H:i:s", time()),
		':id' => $id_admin
	);

	$stmt->execute($colParams);
}

function gerarCodigo()
{
	$numero = 0;
	$sair = false;

	// Verifica se ja esta em uso
	while ($sair === false) {

		// Gera o numero randomico
		$numero = md5(rand(111111, 999999));

		$sql = "SELECT * FROM sessao WHERE (codigo = :codigo)";
		$stmt = DB::prepare($sql);

		$colParams = array(
			':codigo' => $numero,
		);

		$stmt->execute($colParams);
		$res = $stmt->fetchAll();

		if (sizeof($res) == 0) {
			$sair = true;
		}
	}

	// Retorna o numero
	return $numero;
}

function validarSessao()
{
	// Valida se o codigo esta ativo ainda
	$sql = "SELECT * FROM sessao WHERE (codigo = :codigo AND data_limite > :data_limite)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':codigo' => @$_SERVER['HTTP_TOKEN'],
		':data_limite' => date("Y-m-d H:i:s", time()),
	);

	$stmt->execute($colParams);
	$retorno = $stmt->fetchAll();

	if ($retorno) {

		$retornoArray = [];
		$retornoArray['admin'] = $retorno[0];

		return array("sessao" => $retornoArray);
	}

	return null;
}

function isBlank($vlr)
{
	if ($vlr == "undefined" || $vlr == null || $vlr == "" || $vlr == -1) {
		return true;
	}

	return false;
}

function isNotBlank($vlr)
{
	return !isBlank($vlr);
}

?>