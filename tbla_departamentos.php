<?php

$app->get("/departamentos", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM departamentos ORDER BY titulo ASC";
	$stmt = DB::prepare($sql);

	$stmt->execute();
	$retorno = $stmt->fetchAll();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson([
			'error'=>'Nenhum registro encontrado',
			'code'=>404
		], 404);
	}

	exit();
});

$app->get("/departamentos/{id}", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM departamentos WHERE (id = :id)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':id' => $args['id']
	);

	$stmt->execute($colParams);
	$retorno = $stmt->fetch();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson([
			'error'=>'Nenhum registro encontrado',
			'code'=>404
		], 404);
	}

	exit();
});

?>