<?php

$app->get("/config", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM config";
	$stmt = DB::prepare($sql);

	$stmt->execute();
	$retorno = $stmt->fetch();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson([
			'error'=>'Nenhum registro encontrado',
			'code'=>404
		], 404);
	}

	exit();
});

$app->get("/pedidos_situacao", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM pedidos_situacao ORDER BY id ASC";
	$stmt = DB::prepare($sql);

	$stmt->execute();
	$retorno = $stmt->fetchAll();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson([
			'error'=>'Nenhum registro encontrado',
			'code'=>404
		], 404);
	}

	exit();
});

$app->get("/estados", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM estados ORDER BY titulo ASC";
	$stmt = DB::prepare($sql);

	$stmt->execute();
	$retorno = $stmt->fetchAll();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson([
			'error'=>'Nenhum registro encontrado',
			'code'=>404
		], 404);
	}

	exit();
});

$app->get("/cidades/{id_estado}", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM cidades WHERE (id_estado = :id_estado) ORDER BY titulo ASC";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':id_estado' => $args['id_estado']
	);

	$stmt->execute($colParams);
	$retorno = $stmt->fetchAll();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson([
			'error'=>'Nenhum registro encontrado',
			'code'=>404
		], 404);
	}

	exit();
});

?>