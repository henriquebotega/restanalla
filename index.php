<?php
session_start();
header("Content-Type: application/json");

define("DB_HOST", "192.185.215.168");
define("DB_USER", "alvoi435_analla");
define("DB_PASSWORD", "alvoi435_analla");
define("DB_NAME", "alvoi435_analla");

@header("Access-Control-Allow-Origin: *");
@header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, PATCH, OPTIONS");
@header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization, token");
@header("Access-Control-Max-Age: 86400");

include_once 'db.php';
include_once 'vendor/autoload.php';

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;

$app = new Slim\App(["settings" => $config]);

$app->get('/', function ($request, $response, $args) {
	$response->write("Rest GET Online!");
	return $response;
});

/* CORS */
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization, token')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
            ->withHeader('Access-Control-Request-Headers', 'token');
});

// Funções diversas
include_once "tbla_funcoes.php";
include_once "tbla_cadastros.php";
include_once "tbla_produtos.php";
include_once "tbla_departamentos.php";
include_once "tbla_pedidos.php";
include_once "tbla_outros.php";

// INTERCEPTOR
$app->add(function ($request, $response, $next) {
    if($request->getUri()->getPath() !== 'admin/logar'){
        if (isBlank(validarSessao())) {
            return $response->withJson([
                'error'=>'A sessao expirou!',
                'code'=>403
            ], 403);
        }
    }

    return $next($request, $response);
});

$app->run();

?>
