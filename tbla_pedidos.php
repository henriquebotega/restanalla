<?php

$app->get("/pedidos", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM pedidos ORDER BY data DESC";
	$stmt = DB::prepare($sql);

	$stmt->execute();
	$retorno = $stmt->fetchAll();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson([
			'error'=>'Nenhum registro encontrado',
			'code'=>404
		], 404);
	}

	exit();
});

$app->get("/pedidos/{id}", function ($request, $response, $args)  {
	$data = $request->getParsedBody();

	$sql = "SELECT * FROM pedidos WHERE (id = :id)";
	$stmt = DB::prepare($sql);

	$colParams = array(
		':id' => $args['id']
	);

	$stmt->execute($colParams);
	$retorno = $stmt->fetch();

	if ($retorno) {
		echo json_encode(array("retorno" => $retorno));
	} else {
		return $response->withJson([
			'error'=>'Nenhum registro encontrado',
			'code'=>404
		], 404);
	}

	exit();
});

?>